import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ArrayForSort {
    private int count = 0;
    private int max = 0;
    private int maxCount = 0;
    private int min = 0;
    private int minCount = 0;
    private int[] currArr;
    boolean show = false;
    int m;
    static int choice = 0;
    static int counter = 0;
    static List<Integer> counterList;

    ArrayForSort(int count, int max, int min, boolean show){
        this.count = count;
        this.max = max+1;
        this.min = min;
        this.show = show;
        setArr();
        startSort();
    }

    private void setArr(){
        if(show)System.out.print("Изначальный массив: [ ");
        currArr = new int[count];
        for(int i = 0; i < count; i++){
            currArr[i] = (int)(min+((max-min)*Math.random()));
            if(show){
                if(i+1!=count) {
                    System.out.print(currArr[i] + ", ");
                }
                else System.out.println(currArr[i] + " ]");
            }
        }


    }
    private void startSort(){
        if(choice == 0){Scanner in = new Scanner(System.in);

        System.out.println("Выберите алгоритм сортировки: ");
        System.out.println("1 - быстрая");
        System.out.println("2 - слиянием");
        System.out.println("3 - поразрядная");
        choice = in.nextInt();
        in.close();}
        switch (choice) {
            case 1 -> {
                quickSort(currArr, 0, currArr.length - 1);
                break;
            }
            case 2 -> {
                sliyanieSort(currArr, 0, currArr.length - 1);
                break;
            }
            case 3 -> {
                radixSort(currArr);
                break;
            }
            default -> System.out.println("Incorrect input");
        }
    }
    public static void save(){
        try(FileWriter writer = new FileWriter("result.txt", true))
        {
            writer.write(counterList.toString());
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
    private void showArray(int[] array){
        System.out.print("[ ");
        for(int element = 0; element < array.length; element++){
            if(element+1!=array.length) {
                System.out.print(array[element] + ", ");
            }
            else System.out.println(array[element] + " ]");
        }
    }


    private void quickSort(int[] array, int low, int high) {
        if (array.length == 0) {
            counter++;//сравнение
            return;
        }
        counter++;//сравнение

        if (low >= high) {
            counter++;//сравнение
            return;
        }
        counter++;//сравнение

        int middle = low + (high - low) / 2;
        int opora = array[middle];
        counter += 4;//присваивание нескольких операций
        counter++;//присваивание

        int i = low, j = high;
        counter +=2;//присваивания
        while (i <= j) {
            counter++;//сравнение
            while (array[i] < opora) {
                counter++;//сравнение
                i++;
                counter++;//инкремент
            }
            counter++;//сравнение

            while (array[j] > opora) {
                counter++;//сравнение
                j--;
                counter++;//декремент
            }
            counter++;//сравнение

            if (i <= j) {
                if(show) System.out.println(array[i] + " >= " + array[j]);
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
                counter += 5;//все операции
            }
            else if(show)System.out.println("pivot in correct place");
            counter++;//сравнение
            if(show)showArray(array);
        }

        if (low < j)
            counter++;//сравнение
            quickSort(array, low, j);

        if (high > i)
            counter++;//сравнение
            quickSort(array, i, high);
    }
    private void sliyanieSort(int[] array, int low, int high) {
        counter++;//сравнение
        if (high <= low) {
            return;
        }
        int mid = low + (high - low) / 2;
        counter += 4;//все
        sliyanieSort(array, low, mid);
        counter++;//mid + 1
        sliyanieSort(array, mid + 1, high);

        int[] buf = Arrays.copyOf(array, array.length);
        counter++;//присваивание
        counter++;//присваивание к
        for (int k = low; k <= high; k++) {
            counter++;//k<=high
            buf[k] = array[k];
            counter++;//присвавание
            counter++;//инкремент
        }
        counter++;//k<=high

        int i = low, j = mid + 1;
        counter += 3;//все
        for (int k = low; k <= high; k++) {
            counter++;//k<=high
            counter++;//if
            if (i > mid) {
                array[k] = buf[j];
                j++;
                counter += 2;//
            } else if (j > high) {
                array[k] = buf[i];
                i++;
                counter += 3;
            } else if (buf[j] < buf[i]) {
                array[k] = buf[j];
                j++;
                counter += 4;
            } else {
                array[k] = buf[i];
                i++;
                counter += 4;
            }
            counter++;//k++

        }
        if(show)showArray(array);

    }
    private void radixSort(int[] array) {

        List<Integer>[] buckets = new ArrayList[10];
        counter++;
        counter++;//i=0
        for (int i = 0; i < buckets.length; i++) {
            counter++;//i<
            buckets[i] = new ArrayList<Integer>();
            counter++;//=
            counter++;//i++
        }
        counter++;//i<

        // sort
        boolean flag = false;
        counter++;
        int tmp = -1, divisor = 1;
        counter++;
        counter++;

        counter++;//!flag
        while (!flag) {
            flag = true;
            counter++;
            // split array between lists
            for (Integer i : array) {
                tmp = i / divisor;
                counter++;
                counter++;
                buckets[tmp % 10].add(i);
                counter++;
                counter++;
                counter++;//if
                if (flag && tmp > 0) {
                    flag = false;
                    counter++;
                }
            }
            // empty lists into array
            int a = 0;
            counter++;
            counter++;//b=0
            for (int b = 0; b < 10; b++) {
                counter++;//b<
                for (Integer i : buckets[b]) {
                    array[a++] = i;
                    counter++;
                    counter++;//i++
                }
                buckets[b].clear();
                counter++;
                counter++;//b++
            }
            // move to next digit
            divisor *= 10;
            counter++;
            if(show)showArray(array);
        }
    }
}
