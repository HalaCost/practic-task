import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {



    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("1.Режим визуализаци алгоритма:");
        System.out.println("2.Режим изучения алгоритмической сложности");
        System.out.println("Выберите режим:");
        int choice = in.nextInt();
        switch (choice){
            case 1:{
                robota();
                break;
            }
            case 2:{
                slozhnost();
                break;
            }
            default:
                System.out.println("Incorrect input");
        }

        in.close();
    }
    public static void robota(){

        int count;
        int max;
        int min;

        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество элементов: ");
        count = in.nextInt();
        System.out.println("Введите максимальное значение элементов: ");
        max = in.nextInt();
        System.out.println("Введите минимальное значение элементов: ");
        min = in.nextInt();
        ArrayForSort currentArray = new ArrayForSort(count,max, min, true);

        in.close();
    }
    public static void slozhnost(){
        ArrayForSort currArray;

        int max;
        int maxCount;
        int min;
        int minCount;
        int m;

        try(FileWriter writer = new FileWriter("result.txt", false))
        {
            writer.write("");
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
        Scanner in = new Scanner(System.in);

        System.out.println("Введите максимальное количество элементов: ");
        maxCount = in.nextInt();
        System.out.println("Введите минимальное количество элементов: ");
        minCount = in.nextInt();
        System.out.println("Введите максимальное значение элементов: ");
        max = in.nextInt();
        System.out.println("Введите минимальное значение элементов: ");
        min = in.nextInt();
        System.out.println("Количество повторений: ");
        m = in.nextInt();
        int count = minCount;
        ArrayForSort.counterList = new ArrayList<>();
        for (int i = 0; i< m; i++) {
            while (count <= maxCount){
                for(int j = 0; j<m; j++) {
                    currArray = new ArrayForSort(count, max, min, false);
                }
                ArrayForSort.counter /= m;
                ArrayForSort.counterList.add(ArrayForSort.counter);
                ArrayForSort.counter = 0;
                count += 10;
            }

            count = minCount;
            System.out.println(ArrayForSort.counterList.toString());
            ArrayForSort.save();
            ArrayForSort.counterList.clear();
        }

        in.close();
    }
}
